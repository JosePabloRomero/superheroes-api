const express = require('express');
const { getAllHeroes, getSingleHero, addHero, updateHero, deleteHero } = require('../controllers/hero_controller');
const { body } = require("express-validator");

const router = express.Router({ mergeParams: true });

const heroeCreationValidators = [
    body('name').notEmpty().isString(),
    body('publisher').notEmpty().isString(),
    body().custom(body => {
        const keys = ['name', 'publisher'];
        return Object.keys(body).every(key => keys.includes(key));
    }).withMessage('Se agregaron parametros extra')
]

const heroUpdateValidators = [
    body('name').notEmpty().isString(),
    body('publisher').notEmpty().isString(),
    body().custom(body => {
        const keys = ['name', 'publisher'];
        return Object.keys(body).every(key => keys.includes(key));
    }).withMessage('Se agregaron parametros extra')
]

router.get('/heroes', getAllHeroes);
router.get('/heroes/:idHero', getSingleHero);
router.post('/heroes', heroeCreationValidators, addHero);
router.put('/heroes/:idHero', heroUpdateValidators, updateHero);
router.delete('/heroes/:idHero', deleteHero);

module.exports = router;