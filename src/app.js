const express = require('express');
const morgan = require('morgan');
const cors = require("cors");
const app = express();
const heroe_routes = require("./routes/hero_routes");

app.use(morgan('dev'));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));

app.use('/api', heroe_routes);

module.exports = app;