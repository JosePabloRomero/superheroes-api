const { db } = require('../firebase');
const { validationResult } = require("express-validator");

const getAllHeroes = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const query = db.collection("super_heroes");
        const querySnapshot = await query.get();
        const docs = querySnapshot.docs;
        const response = docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }));
        return res.status(200).json(response);
    } catch (error) {
        return res.status(500).json();
    }
};

const getSingleHero = async (req, res) => {
    try {
        const id = req.params.idHero;
        const Hero = db.collection("super_heroes").doc(id);
        const data = await Hero.get();
        if (!data.exists) {
            res.status(404).send('El heroe no fue encontrado');
        } else {
            const response = data.data();
            return res.status(200).json(response);
        }
    } catch (error) {
        return res.status(500).json();
    }
};

const addHero = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const data = req.body;
        await db.collection("super_heroes").doc().set(data);
        res.send("El Héroe fue creado correctamente");
        return res.status(204);
    } catch (error) {
        console.log(error);
        return res.status(500).send(error);
    }
};

const updateHero = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            console.log(errors.array())
            return res.status(400).json({ errors: errors.array() });
        }
        const id = req.params.idHero;
        const data = req.body;
        console.log(data);
        const Hero = db.collection("super_heroes").doc(id);
        await Hero.update(data);
        res.send("El héroe fue actualizado correctamente");
        return res.status(200).json();
    } catch (error) {
        return res.status(500).json();
    }
};

const deleteHero = async (req, res) => {
    try {
        const id = req.params.idHero;
        const doc = db.collection("super_heroes").doc(id);
        await doc.delete();
        return res.status(200).send("El usuario fue borrado con exito");
    } catch (error) {
        return res.status(500).json();
    }
};

module.exports = {
    getAllHeroes,
    getSingleHero,
    addHero,
    updateHero,
    deleteHero
}