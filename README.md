# SuperHeroes API
Esta es una API Rest básica, construída con node.js y firebase como fuente principal.
La aplicación cuenta con una única colección, llamada "super_heroes", el cual cuenta con dos campos (el nombre del super heroe, y su pubicador)

Para el desarrollo de está se utilizaron los siguientes módulos de node:

1.  express y express-validator
2.  dotenv
3.  firebase-admin
4.  morgan
5.  cors
6.  nodemon

Para poder correr la aplicación se debe de cambiar el path del archivo '.env' con la ruta completa del archivo firebase.json.
Esta información es sensible, y por mótivos de practicidad se subió al repositorio.

Finalmente se corre con el script 'npm run dev''.
